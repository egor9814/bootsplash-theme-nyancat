# bootsplash-theme-nyancat

Manjaro Bootsplash Theme with NyanCat

# Installation & configuration

`git clone https://gitlab.com/egor9814/bootsplash-theme-nyancat.git`

`cd bootsplash-theme-nyancat`

Run `chmod +x bootsplash-packer pack.sh`

Run `makepkg` to create Arch package and install it with `pacman -U %packagename%`

Append `bootsplash-nyancat` hook in the end of `HOOKS` string of /etc/mkinitcpio.conf

Add to end `bootsplash.bootfile=bootsplash-themes/nyancat/bootsplash` into `GRUB_CMDLINE_LINUX_DEFAULT` string of `/etc/default/grub`

Run `sudo mkinitcpio -P && sudo update-grub` to update initial ram disk and grub configuration
